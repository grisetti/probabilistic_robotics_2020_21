1
disp("loading icp2d.m")
source "common_stuff.m"

function [e,J]=errorAndJacobian(x,p,z)
 t=x(1:2);
 my_alpha=x(3);
 R=rotation2D(my_alpha);
 z_hat=R*p+t;
 e=z_hat-z;
 J=zeros(2,3);
 J(1:2,1:2)=eye(2);
 J(1:2,3)=rotation2Dgradient(my_alpha)*p;
endfunction;

function [e,J]=errorAndJacobianBearing(x,p,z)
 t=x(1:2);
 my_alpha=x(3);
 R=rotation2D(my_alpha);
 p_hat=R*p+t;
 z_hat=atan2(p_hat(2),p_hat(1));
 e=z_hat-z;
 e=atan2(sin(e), cos(e)); %normalize error
 J_icp=zeros(2,3);
 J_icp(1:2,1:2)=eye(2);
 J_icp(1:2,3)=rotation2Dgradient(my_alpha)*p;
 J_atan=J_atan2(p_hat);
 J=J_atan*J_icp;
endfunction;

function [e,J]=errorAndJacobianRange(x,p,z)
 p_delta=p-x;
 z_hat=p_delta' * p_delta; 
 e=z_hat-z;
 J=2*(x-p)';
endfunction;


function [chi, x_new]=icp2dBearing(x,P,Z)
  chi=0; %cumulative chi2
  H=zeros(3,3);  b=zeros(3,1); %accumulators for H and b
  for(i=1:size(P,2))
     p=P(:,i); z=Z(:,i); % fetch point and measurement
     [e,J]=errorAndJacobianBearing(x,p,z); %compute e and J for the point
     H+=J'*J;            %assemble H and B
     b+=J'*e;
     chi+=e'*e;          %update cumulative error
  endfor
  dx=-H\b;               %solve the linear system
  x_new=x+dx;            %apply update

  %normalize theta between -pi and pi
  my_alpha=x(3);
  s=sin(my_alpha); c=cos(my_alpha);
  x(3)=atan2(s,c);
endfunction

function [chi, x_new]=icp2d(x,P,Z)
  chi=0; %cumulative chi2
  H=zeros(3,3);  b=zeros(3,1); %accumulators for H and b
  for(i=1:size(P,2))
     p=P(:,i); z=Z(:,i); % fetch point and measurement
     [e,J]=errorAndJacobian(x,p,z); %compute e and J for the point
     H+=J'*J;            %assemble H and B
     b+=J'*e;
     chi+=e'*e;          %update cumulative error
  endfor
  dx=-H\b;               %solve the linear system
  x_new=x+dx;            %apply update

  %normalize theta between -pi and pi
  my_alpha=x(3);
  s=sin(my_alpha); c=cos(my_alpha);
  x(3)=atan2(s,c);
endfunction

function [chi, x_new]=icp2dRange(x,P,Z)
  chi=0; %cumulative chi2
  H=zeros(2,2);  b=zeros(2,1); %accumulators for H and b
  for(i=1:size(P,2))
     p=P(:,i); z=Z(:,i); % fetch point and measurement
     [e,J]=errorAndJacobianRange(x,p,z); %compute e and J for the point
     H+=J'*J;            %assemble H and B
     b+=J'*e;
     chi+=e'*e;          %update cumulative error
  endfor
  dx=-H\b;               %solve the linear system
  x_new=x+dx;            %apply update
endfunction


%example_function, call with 
disp("[chi_ev, x_ev]=testICP2D(5,[20 -10 pi/3]',10)")

function [chi_evolution, x_evolution]=testICP2D(num_points, target_transform, iterations)
  chi_evolution=zeros(iterations,1);
  x_evolution=zeros(3,iterations);
  scale=10;
  P=generateRandomPoints(scale, num_points);
  Z=transformPoints(P,target_transform);
  x_current=zeros(3,1);
  for (i=1:iterations)
      [chi,x_current]=icp2d(x_current,P,Z);
      chi_evolution(i)=chi;
      x_evolution(:,i)=x_current;
  endfor;
endfunction

%function for my lil PRers
function [chi_evolution, x_evolution]=testICP2DBearing(num_points, target_transform, iterations)
  chi_evolution=zeros(iterations,1);
  x_evolution=zeros(3,iterations);
  scale=10;
  P=generateRandomPoints(scale, num_points);
  P_transformed=transformPoints(P,target_transform);
  Z=getBearing(P_transformed);
  x_current=zeros(3,1);
  for (i=1:iterations)
      [chi,x_current]=icp2dBearing(x_current,P,Z);
      chi_evolution(i)=chi;
      x_evolution(:,i)=x_current;
  endfor;
endfunction

%function for my lil PRers
function [chi_evolution, x_evolution]=testICP2DRange(num_points, target_transform, iterations)
  chi_evolution=zeros(iterations,1);
  x_evolution=zeros(2,iterations);
  scale=10;
  P=generateRandomPoints(scale, num_points);
  transform = [target_transform; 0];
  P_transformed=transformPoints(P,transform);
  Z=getSquaredNorm(P_transformed);
  x_current=zeros(2,1);
  for (i=1:iterations)
      [chi,x_current]=icp2dRange(x_current,P,Z);
      chi_evolution(i)=chi;
      x_evolution(:,i)=x_current;
  endfor;
endfunction

