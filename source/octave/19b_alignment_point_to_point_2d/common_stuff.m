1
disp("loading common_stuff.m")

function R=rotation2D(theta)
  s=sin(theta);
  c=cos(theta);
  R=[c -s;
     s  c];
endfunction;

function Rp=rotation2Dgradient(theta)
  s=sin(theta);
  c=cos(theta);
  Rp=[-s -c;
      c -s];
endfunction;

function T=v2t(v)
  T=eye(3);
  T(1:2,1:2)=rotation2D(v(3));
  T(1:2,3)=v(1:2);
endfunction;

function P=generateRandomPoints(scale, number)
	 P=(rand(2,number)-ones(2,number)*0.5)*scale;
endfunction;

function P_new=transformPoints(P,x)
  t=x(1:2);
  theta=(x(3));
  R=rotation2D(theta);
  P_new=R*P+repmat(t,1,size(P,2));
endfunction

function Z=getBearing(P)
  Z=atan2(P(2,:), P(1,:))
endfunction

function Z=getSquaredNorm(P)
  Z=zeros(1,size(P,2));
  for (i=1:size(P,2))
    p=P(:,i);
    Z(i)=p'*p;
  endfor
endfunction

%%%
% J=1./(x^2+y^2) * [-y x];
%%%
function J=J_atan2(p)
   J = 1./(p'*p) * [-p(2) p(1)];
endfunction
                   
