void importance_sampling(int* indices, const double* weights, int n){
  double acc=0;
  const double* w = weights;
  for (int i=0; i<n; i++, w++) 
    acc+= *w;
  
  double inverse_acc = 1./acc;
  double cumulative_value=0;
  double step = 1./n;
  double threshold = step * drand48();
  int* idx = indices;
  
  w=weights;
  for (int i=0; i<n; i++, w++){
    cumulative_value += (*w) *inverse_acc;
    while(cumulative_value>threshold){
      *idx = i;
      idx++;
      threshold += step;
    }
  }
}
