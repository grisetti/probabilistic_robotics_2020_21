#pragma once
#include <vector>
#include <map>
#include <iostream> 
#include <fstream>

#include <Eigen/Core>
#include <Eigen/LU>
using namespace std;

struct LandmarkObservation {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  int id;
  Eigen::Vector2f mean;
  Eigen::Matrix2f cov;
};

struct ControlInput {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  Eigen::Vector3f mean;
  Eigen::Matrix3f cov;
};

struct State {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  Eigen::Vector3f mean;
  Eigen::Matrix3f cov;
};


typedef std::map<int, Eigen::Vector2f> LandmarkMap;
typedef std::vector<LandmarkObservation> ObservationsVector;
typedef std::map<int, ObservationsVector> ObservationsMap;

class EkfLandmarks {
 public:
  void load(const char* filename);
  void predict(int step);
  void update(int step);

  State state;
  Eigen::Vector3f odom;

  //Input Data
  std::map<int, Eigen::Vector3f> gt_robotPoses;
  LandmarkMap gt_landmarks;
  std::map<int, ControlInput> control_inputs;
  ObservationsMap observations;
  
};

