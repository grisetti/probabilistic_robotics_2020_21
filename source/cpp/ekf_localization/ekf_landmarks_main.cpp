#include "qapplication.h"

#include "map_viewer.h"
#include "ekf_landmarks.h"
#include <unistd.h>
QApplication* app;
MapViewer *viewer;

void waitForKey(){
  //waiting for key press
  while (true){
    app->processEvents();
    QKeyEvent* event =viewer->lastKeyEvent();
    if (event){
      viewer->keyEventProcessed();
      break;
    }
    usleep(100000);
    viewer->updateGL();
  }
}

int main(int argc, char** argv) {
  if (argc<2) {
    cerr << "Error: you must provide data in g2o format." << endl;
    return 0;
  }

  EkfLandmarks ekfl;
  ekfl.load(argv[1]);

  app= new QApplication(argc,argv);
  viewer = new MapViewer(&ekfl);
  viewer->show();
  
  //Initial position
  std::map<int, Eigen::Vector3f>::iterator itinitial = ekfl.gt_robotPoses.begin();
  std::cerr << "Initial step: " << itinitial->first << std::endl;
  viewer->setCurrentStep(itinitial->first);
  viewer->setStatus(MapViewer::Init);

  itinitial++;

  waitForKey();
  for (std::map<int, Eigen::Vector3f>::iterator it= itinitial; it!=ekfl.gt_robotPoses.end(); it++){
    int current_step = it->first;
    std::cerr << "Current step: " << current_step << std::endl;
    viewer->setCurrentStep(current_step);
    viewer->setStatus(MapViewer::Init);
    
    waitForKey();
    viewer->setStatus(MapViewer::Predict);
    ekfl.predict(current_step);

    waitForKey();
    std::cerr << "Observe." << std::endl;
    std::cerr << "Number of observations: " << ekfl.observations[current_step].size() << std::endl << std::endl;
    viewer->setStatus(MapViewer::Observe);

    waitForKey();
    ekfl.update(current_step);
    viewer->setStatus(MapViewer::Update);

    waitForKey();
  }


  app->exec();



}
