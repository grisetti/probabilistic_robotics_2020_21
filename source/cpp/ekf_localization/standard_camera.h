#include "qglviewer.h"

class StandardCamera: public qglviewer::Camera {
 public:
 StandardCamera(): _standard(true) {}
  qreal zNear() const;
  qreal zFar() const;
  inline bool standard() const { return _standard; }  
  inline void setStandard(bool s) { _standard = s; }

 protected:
  bool _standard;
};
