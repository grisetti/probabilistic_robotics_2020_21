#include <Eigen/Core>
typedef Eigen::Matrix<float, 2, 3> Matrix23f;

inline float normalize_theta(float theta)
{
  if (theta >= -M_PI && theta < M_PI)
    return theta;
  
  float multiplier = floor(theta / (2*M_PI));
  theta = theta - multiplier*2*M_PI;
  if (theta >= M_PI)
    theta -= 2*M_PI;
  if (theta < -M_PI)
    theta += 2*M_PI;

  return theta;
}

inline Eigen::Vector2f composeTransformWithPoint2D(const Eigen::Vector3f& transf, const Eigen::Vector2f& point2D){
  Eigen::Vector2f result;
  Eigen::Matrix2f rot;
  rot(0,0) = cos(transf.z()); 
  rot(0,1) = -sin(transf.z());
  rot(1,0) = sin(transf.z()); 
  rot(1,1) = cos(transf.z());

  Eigen::Vector2f poseXY(transf.x(),transf.y());
  result = poseXY + rot*point2D;
  return result;
}

inline Matrix23f jacobian1CompositionTransformPoint2D(const Eigen::Vector3f& transf, const Eigen::Vector2f& point2D){

  float cosTheta = cos(transf.z());
  float sinTheta = sin(transf.z());
  Matrix23f J1;
  J1 << 1, 0, -point2D.x()*sinTheta-point2D.y()*cosTheta,
        0, 1,  point2D.x()*cosTheta-point2D.y()*sinTheta;

  return J1;
}

inline Eigen::Matrix2f jacobian2CompositionTransformPoint2D(const Eigen::Vector3f& transf, const Eigen::Vector2f& point2D){

  float cosTheta = cos(transf.z());
  float sinTheta = sin(transf.z());
  Eigen::Matrix2f J2;
  J2 << cosTheta, -sinTheta,
        sinTheta,  cosTheta;

  return J2;
}

inline Eigen::Vector3f composeTwoTransforms(const Eigen::Vector3f& transf1, const Eigen::Vector3f& transf2){

  float cosTheta = cos(transf1.z());
  float sinTheta = sin(transf1.z());
  Eigen::Vector3f newTransform(transf1.x() + transf2.x() * cosTheta - transf2.y() * sinTheta,
			       transf1.y() + transf2.x() * sinTheta + transf2.y() * cosTheta,
			       normalize_theta(transf1.z() + transf2.z()));

  return newTransform;
}

inline Eigen::Matrix3f jacobian1CompositionTwoTransforms(const Eigen::Vector3f& transf1, const Eigen::Vector3f& transf2){

  float cosTheta = cos(transf1.z());
  float sinTheta = sin(transf1.z());
  Eigen::Matrix3f J1;
  J1 << 1, 0, -transf2.x()*sinTheta-transf2.y()*cosTheta,
        0, 1,  transf2.x()*cosTheta-transf2.y()*sinTheta,
        0, 0, 1;

  return J1;
}


inline Eigen::Matrix3f jacobian2CompositionTwoTransforms(const Eigen::Vector3f& transf1, const Eigen::Vector3f& transf2){

  float cosTheta = cos(transf1.z());
  float sinTheta = sin(transf1.z());
  Eigen::Matrix3f J2;
  J2 << cosTheta, -sinTheta, 0,
        sinTheta,  cosTheta, 0,
        0, 0, 1;

  return J2;
}

inline Eigen::Vector3f inverseTransform(const Eigen::Vector3f& transf){
  float cosTheta = cos(transf.z());
  float sinTheta = sin(transf.z());

  Eigen::Vector3f inverseT(-transf.x()*cosTheta - transf.y()*sinTheta,
			   transf.x()*sinTheta - transf.y()*cosTheta,
			  -transf.z());

  return inverseT;
}

inline Eigen::Matrix3f jacobianInverseTransform(const Eigen::Vector3f& transf){

  float cosTheta = cos(transf.z());
  float sinTheta = sin(transf.z());

  Eigen::Matrix3f Jinv;
  Jinv << -cosTheta, -sinTheta, transf.x()*sinTheta - transf.y()*cosTheta,
           sinTheta, -cosTheta, transf.x()*cosTheta + transf.y()*sinTheta,
           0, 0, -1;

  return Jinv;
}
