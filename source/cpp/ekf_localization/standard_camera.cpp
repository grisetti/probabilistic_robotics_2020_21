#include "standard_camera.h"
  
qreal StandardCamera::zNear() const {
  if(_standard) { return 0.001f; } 
  else { return Camera::zNear(); } 
}

qreal StandardCamera::zFar() const {  
  if(_standard) { return 10000.0f; } 
  else { return Camera::zFar(); }
}
