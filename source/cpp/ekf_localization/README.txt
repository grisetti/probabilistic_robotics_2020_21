This folder contains
- an implementation of a Extended Kalman Filter for mobile robot localization in a 2D-point landmark-based map

requires:
- libeigen3-dev
- libqt4-dev
- qt4-qmake
- libqglviewer-qt4-dev

To compile:

cd ekf_localizer
mkdir build
cd build
cmake ../
make

in /bin and /lib are the binaries
in /data there are examples in g2o format.

Example of use (from ekf_localizer directory): ./bin/ekf_localizer data/sim100.g2o
Press any key to visualize the simulation step by step
