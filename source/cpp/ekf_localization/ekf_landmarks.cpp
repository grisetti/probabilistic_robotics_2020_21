#include "ekf_landmarks.h"
#include "transforms_helper.h"

void EkfLandmarks::load(const char* filename) {
  ifstream is(filename);

  std::cerr << "opening filename [" << filename << "]" << std::endl;
  if (!is.good())
    return;
  
  const size_t line_length = 1024;
  char* buf=new char[line_length];
  while (is.good()){
    is.getline(buf, line_length);
    if (! is.good())
      break;
    std::cerr << "parsing line [" << buf <<"]" << std::endl;
    
    istringstream ls(buf);
    string tag;
    ls >> tag;
    if (tag == "VERTEX_SE2"){
      int poseid;
      ls >> poseid;
      
      Eigen::Vector3f robotpose;
      ls >> robotpose.x() >> robotpose.y() >> robotpose.z();
      gt_robotPoses.insert(std::make_pair(poseid,robotpose));
    }
    
    if (tag == "VERTEX_XY"){
      int landmarkid;
      ls >> landmarkid;
      
      Eigen::Vector2f landmarkpose;
      ls >> landmarkpose.x() >> landmarkpose.y();

      gt_landmarks.insert(std::make_pair(landmarkid, landmarkpose));
    }

    if (tag == "EDGE_SE2"){
      //Control input from step 'idfrom' to step 'idto'
      int idfrom, idto;
      ls >> idfrom >> idto;

      ControlInput control_u;
      Eigen::Vector3f u;
      ls >> control_u.mean.x() >> control_u.mean.y() >> control_u.mean.z();

      Eigen::Matrix3f inf;
      ls >> inf(0,0) >> inf(0,1) >> inf(0,2) 
	 >> inf(1,1) >> inf(1,2) >> inf(2,2);
      inf(1,0) = inf(0,1);
      inf(2,0) = inf(0,2);
      inf(2,1) = inf(1,2);
      
      control_u.cov = inf.inverse();

      if (idto == idfrom+1)
	control_inputs.insert(std::make_pair(idto, control_u));
    }

    if (tag == "EDGE_SE2_XY"){
      //Observation of a landmark from step 'idfrom'
      int idfrom;
      ls >> idfrom;

      LandmarkObservation observation;
      ls >> observation.id;
      ls >> observation.mean.x() >> observation.mean.y();
      Eigen::Matrix2f inf;
      ls >> inf(0,0) >> inf(0,1) >> inf(1,1);
      inf(1,0) = inf(0,1);

      observation.cov = inf.inverse();

      ObservationsMap::iterator it = observations.find(idfrom);
      if (it == observations.end()){
	//First landmark from this step
	ObservationsVector v;
	v.push_back(observation);
	observations.insert(std::make_pair(idfrom, v));
      }else {
	observations[idfrom].push_back(observation);
      }
    }
  }


  //Initial robot state
  //We know the exact initial position
  state.mean = gt_robotPoses.begin()->second;
  //Zero uncertainty
  state.cov = Eigen::Matrix3f::Zero(); 

  odom = state.mean;
}

void EkfLandmarks::predict(int step){
  ControlInput u = control_inputs[step];
  cerr << "u: " << u.mean.transpose() << endl;

  Eigen::Vector3f newMean = composeTwoTransforms(state.mean,  u.mean);
  Eigen::Matrix3f J1 = jacobian1CompositionTwoTransforms(state.mean, u.mean);
  Eigen::Matrix3f J2 = jacobian2CompositionTwoTransforms(state.mean, u.mean);

  Eigen::Matrix3f newCov = J1*state.cov*J1.transpose() + J2*u.cov*J2.transpose();

  state.mean = newMean;
  state.cov  = newCov;

  Eigen::Vector3f newOdom = composeTwoTransforms(odom,  u.mean);
  odom = newOdom;
  
  std::cerr << "Predict:\n" 
	    << "Mean:\n" << state.mean
	    << "\nCovariance:\n"<< state.cov << std::endl << std::endl;  
}

void EkfLandmarks::update(int step){
  ObservationsVector obsStep = observations[step];

  Eigen::Vector3f meaninv = inverseTransform(state.mean);
  Eigen::Matrix3f Jinv    = jacobianInverseTransform(state.mean);
  
  Eigen::MatrixXf H(2*obsStep.size(), 3); H.setZero();
  Eigen::MatrixXf R(2*obsStep.size(), 2*obsStep.size()); R.setZero();
  Eigen::MatrixXf z(2*obsStep.size(), 1); z.setZero();
  Eigen::MatrixXf h(2*obsStep.size(), 1); h.setZero();
  
  for (size_t i = 0; i<obsStep.size(); i++){
    LandmarkObservation zi = obsStep[i];

    Eigen::Vector2f gtLandmarkPose = gt_landmarks[zi.id];

    Eigen::Vector2f hi  = composeTransformWithPoint2D(meaninv, gtLandmarkPose);
    Matrix23f J1p       = jacobian1CompositionTransformPoint2D(meaninv, gtLandmarkPose);
    
    H.block<2,3>(2*i,0) = J1p * Jinv;
    R.block<2,2>(2*i,2*i) = zi.cov;
    
    z.block<2,1>(2*i,0) = zi.mean;
    h.block<2,1>(2*i,0) = hi;
  }

  //Innovation
  Eigen::MatrixXf innovation = z - h;
  Eigen::MatrixXf S = H * state.cov * H.transpose() + R;
  
  //Kalman gain
  Eigen::MatrixXf K = state.cov * H.transpose() * S.inverse();
  
  // std::cerr << "\nInnovation:\n" << innovation
  // 	    << "\nInnovationCovariance:\n" << S
  //   	    << "\nKalmanGain:\n"<< K << std::endl;  

  //Kalman update
  state.mean = state.mean + K * innovation;
  state.mean.z() = normalize_theta(state.mean.z());
  state.cov  = state.cov - K * H * state.cov;
 
  
  std::cerr << "Update:\n" 
	    << "Mean:\n" << state.mean
	    << "\nCovariance:\n"<< state.cov << std::endl << std::endl;  
}
