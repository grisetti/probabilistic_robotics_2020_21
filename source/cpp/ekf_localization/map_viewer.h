#include "simple_viewer.h"
#include "ekf_landmarks.h"

class MapViewer: public SimpleViewer{
public:
  MapViewer(EkfLandmarks* ekfl);
  enum Status {Init=0x0, Predict=0x1, Observe=0x2, Update=0x3};

  void draw();
  void drawState();
  void drawRobot(const Eigen::Vector3f& pose, const Eigen::Vector3f& color = Eigen::Vector3f::Zero());
  void drawGTLandmarks();
  void drawObservations();
  void drawAllObservations();
  void drawCovariance2D(const Eigen::Vector3f& pose, const Eigen::Matrix2f& covariance, const Eigen::Vector3f& color = Eigen::Vector3f::Zero());
  inline Status status() const {return _status;}
  inline void setStatus(Status s) {_status=s;}
  inline void setCurrentStep(int step_){_current_step=step_;}
protected:
  EkfLandmarks* _ekfl;
  Status _status;
  int _current_step;
};
