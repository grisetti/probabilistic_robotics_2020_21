#include "map_viewer.h"
#include "transforms_helper.h"

#include <Eigen/Eigenvalues>

MapViewer::MapViewer(EkfLandmarks* ekfl){
  _ekfl = ekfl;
  _status=Init;
  _current_step=-1;
}

void MapViewer::draw(){
  drawGTLandmarks();
  drawState();
  switch(_status){
  case Init:
    break;
  case Predict:
    break;
  case Observe:
    drawObservations();
    break;
  case Update:
    drawObservations();
    break;
  }
}

void MapViewer::drawState(){
  Eigen::Vector3f poseEstimate = _ekfl->state.mean;
  Eigen::Vector3f colorR(1,0,0);
  drawRobot(poseEstimate, colorR);

  Eigen::Matrix2f covariance = _ekfl->state.cov.block<2,2>(0,0);
  drawCovariance2D(poseEstimate, covariance, colorR);

  Eigen::Vector3f gtRobotPose = _ekfl->gt_robotPoses[_current_step];
  drawRobot(gtRobotPose);

  Eigen::Vector3f colorB(0,0,1);
  drawRobot(_ekfl->odom, colorB);
}

void MapViewer::drawRobot(const Eigen::Vector3f& pose, const Eigen::Vector3f& color){
  glPushMatrix();
  glTranslatef(pose.x(), pose.y(), 0.0f);
  glRotatef(180.0f/M_PI*pose.z(), 0,0,1);

  glPushAttrib(GL_COLOR|GL_POINT_SIZE);
  glBegin(GL_TRIANGLES);
  glColor3f(color.x(), color.y(), color.z());
  float size = 0.2;
  glVertex3f(1.5*size, 0, 0);
  glVertex3f(-size, size, 0);
  glVertex3f(-size, -size, 0);
  glEnd();
  glPopAttrib();

  glPopMatrix(); 
}

void MapViewer::drawGTLandmarks(){
  for (std::map<int, Eigen::Vector2f>::iterator it=_ekfl->gt_landmarks.begin(); it!=_ekfl->gt_landmarks.end(); it++){
      Eigen::Vector2f landmarkpose = it->second;
      //Draw pose
      glPushAttrib(GL_COLOR|GL_POINT_SIZE);
      glPointSize(5.0f);
      glColor3f(0,0,0);
      glBegin(GL_POINTS);
      glVertex3f(landmarkpose.x(), landmarkpose.y(), 0.0f);
      glEnd();
      glPopAttrib();
  }
}

void MapViewer::drawAllObservations(){
  for (ObservationsMap::iterator it = _ekfl->observations.begin(); it!=_ekfl->observations.end(); it++){
    int step = it->first;
    ObservationsVector obs = it->second;

    Eigen::Vector3f robotpose = _ekfl->gt_robotPoses[step];
    for (size_t i = 0; i < obs.size(); i++){
      Eigen::Vector2f obspose = obs[i].mean;
      //Observation pose in world coordinates
      Eigen::Vector2f poseW = composeTransformWithPoint2D(robotpose,obspose);

      //Draw pose
      glPushAttrib(GL_COLOR|GL_POINT_SIZE);
      glPointSize(5.0f);
      glColor3f(0,1,0);
      glBegin(GL_POINTS);
      glVertex3f(poseW.x(), poseW.y(), 0.0f);
      glEnd();
      glPopAttrib();

      Eigen::Vector3f poseDraw(poseW.x(), poseW.y(), 0);
      drawCovariance2D(poseDraw, obs[i].cov);
    }
  }
}

void MapViewer::drawObservations(){
  if (_current_step<0)
    return;
  ObservationsVector obsStep = _ekfl->observations[_current_step];
  
  Eigen::Vector3f robotpose = _ekfl->state.mean;
  for (size_t i = 0; i < obsStep.size(); i++){
    Eigen::Vector2f obspose = obsStep[i].mean;
    //Observation pose in world coordinates
    Eigen::Vector2f poseW = composeTransformWithPoint2D(robotpose,obspose);

    Eigen::Vector2f gtLandmarkpose = _ekfl->gt_landmarks[obsStep[i].id];

    //Draw pose
    glPushAttrib(GL_COLOR|GL_POINT_SIZE);
    glPointSize(5.0f);
    glColor3f(0,1,0);
    glBegin(GL_POINTS);
    glVertex3f(poseW.x(), poseW.y(), 0.0f);
    glEnd();

    glLineWidth(1.5); 
    glColor3f(1.0, 0.5, 0.0);
    glBegin(GL_LINE_STRIP);
    glVertex3f(robotpose.x(), robotpose.y(), 0.0f);
    glVertex3f(poseW.x(), poseW.y(), 0.0f);
    glVertex3f(gtLandmarkpose.x(), gtLandmarkpose.y(), 0);
    glEnd();
    glPopAttrib();

    Eigen::Vector3f poseDraw(poseW.x(), poseW.y(), 0);
    drawCovariance2D(poseDraw, obsStep[i].cov);
  }

}


void MapViewer::drawCovariance2D(const Eigen::Vector3f& pose, const Eigen::Matrix2f& covariance, const Eigen::Vector3f& color){
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix2f> eigenSolver(covariance);
  Eigen::Matrix2f rot = eigenSolver.eigenvectors();
  Eigen::Vector2f singularValues = eigenSolver.eigenvalues();

  float angle = atan2(rot(1,0), rot(0,0));

  glPushMatrix();
  glTranslatef(pose.x(), pose.y(), 0.0f);
  glRotatef(180.0f/M_PI*pose.z(), 0,0,1);

  glRotatef(angle*180.0/M_PI, 0., 0., 1.);
  glScalef(sqrt(singularValues(0)), sqrt(singularValues(1)), 1);

  glColor3f(1.f,0.0f,1.f);
  glColor3f(color.x(),color.y(),color.z());
  glBegin(GL_LINE_LOOP);
  for(int i=0; i<36; i++){
    float rad = i*M_PI/18.0;
    glVertex2f(cos(rad),
	       sin(rad));
  }
  glEnd();

  glPopMatrix();
}
